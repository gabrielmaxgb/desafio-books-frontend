import axios from "axios";
import { history } from '../history';

export const sendLogin = (values) => {
  axios.post('https://books.ioasys.com.br/api/v1/auth/sign-in', values)
  .then(response => {
    const { data, headers } = response;
    if (data) {
      localStorage.setItem('app-token', JSON.stringify(headers.authorization));
      history.push('/home');
    }
  })
};

export const getBooks = (setBookList, page) => {
  axios.get(
    `https://books.ioasys.com.br/api/v1/books/?page=${page}`,
    {
      headers: {
        authorization: `Bearer ${JSON.parse(localStorage.getItem('app-token'))}`,
      }
    }
  )
  .then(response => {
    setBookList(response.data);
  })
}

export const getBookDetail = (id, setBookDetail) => {
  axios.get(
    `https://books.ioasys.com.br/api/v1/books/${id}`,
    {
      headers: {
        authorization: `Bearer ${JSON.parse(localStorage.getItem('app-token'))}`,
      }
    }
  )
  .then(response => {
    setBookDetail(response.data);
  })
}
