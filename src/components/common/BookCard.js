import { Box, Card, CardActionArea, CardMedia, Grid, makeStyles, Typography } from '@material-ui/core';
import React from 'react'

const useStyle = makeStyles({
  card: {
    width: '272px',
    height: '160px',
    background: '#FFFFFF',
    boxShadow: "0px 6px 24px rgba(84, 16, 95, 0.13)",
    borderRadius: '4px',
  },
  media: {
    width: 81,
    height: 122,
  },
  imageSection: {
    minHeight: '100%',
  },
  aboutSection: {
    padding: '0 10px'
  },
  title: {
    fontWeight: 500,
    fontSize: '14px',
    lineHeight: '20px',
    color: '#333333',
  },
  authors: {
    fontWeight: 'normal',
    fontSize: '12px',
    lineHeight: '20px',
    color: '#AB2680',
  },
  about: {
    fontWeight: 'normal',
    fontSize: '12px',
    lineHight: '20px',
    color: '#999999',
  }
})

const BookCard = (props) => {
  const {
    imageUrl,
    title,
    authors,
    pageCount,
    publisher,
    published,
    id
  } = props.book;
  const { clickAction } = props;
  const classes = useStyle();

  const handleActionAreaClick = (e) => {
    // console.log(e)
    clickAction(id);
  }

  return (
    <Card className={classes.card}>
      <CardActionArea onClick={(e) => handleActionAreaClick(e)} style={{ minHeight: '100%', margin: '0 .5rem' }}>
        <Grid container xs={12}>
          <Grid item container justify="center" alignItems="center" xs={4} className={ classes.imageSection }>
            <CardMedia
              className={classes.media}
              image={imageUrl}
              title=""
            />
          </Grid>
          <Grid item container direction="column" justify="space-between" xs={8} className={ classes.aboutSection }>
            <Box>
              <Typography variant="h5" component="p" color="initial" className={ classes.title }>
                { title }
              </Typography>
              {
                authors
                ? (
                  authors.map(author =>
                    <Typography variant="body1" color="initial" className={ classes.authors }>
                      { author }
                    </Typography>
                  )
                ) : null
              }
            </Box>
            <Box>
              <Typography variant="body" component="p" className={ classes.about }>
                { pageCount }
              </Typography>
              <Typography variant="body" component="p" className={ classes.about }>
                { publisher }
              </Typography>
              <Typography variant="body" component="p" className={ classes.about }>
                { published }
              </Typography>
            </Box>
          </Grid>
        </Grid>
      </CardActionArea>
    </Card>
  )
}

export default BookCard;
