import { Box, Button, Grid, makeStyles, Typography } from '@material-ui/core';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import React, { useEffect } from 'react';
import Logo from '../assets/img/Logo';
import * as yup from 'yup';
import './login.css';
import classNames from 'classnames';
import { sendLogin } from '../services/api';

const useStyles = makeStyles({
  loginContainer: {
    backgroundColor: 'transparent',
    minHeight: '100vh',
  },
  logo: {
    marginRight: '.6rem',
  },
  titleTypo: {
    fontWeight: 300,
    fontSize: '28px',
    lineHeight: '40px',
    color: '#FFFFFF',
  },
  formField: {
    lineHeight: '24px',
    color: '#FFFFFF',
    margin: '1rem 0',
    border: 'none',
    background: 'rgba(0, 0, 0, .25)',
    width: '100%',
    minWidth: '270px',
    height: '60px',
    borderRadius: '4px',
    "&.Mui-focused": {
      outline: 'none',
      border: 'none',
    }
  },
  formBtn: {
    backgroundColor: '#FFFFFF',
    borderRadius: '44px',
    fontWeight: 500,
    fontSize: '16px',
    lineHeight: '20px',
    color: '#B22E6F',
    textTransform: 'capitalize',
    marginTop: '1rem',
    padding: '.5rem 1rem',
  },
  loginError: {
    color: '#FFFFFF;',
    padding: '.3rem',
    borderRadius: '8px',
    backgroundColor: 'rgba(255, 255, 255, 0.4)',
    fontWeight: 700,
  }
});

function Login(props) {
  const classes = useStyles()
  const validations = yup.object().shape({
    email: yup.string().email().required(),
    password: yup.string().min(8).required(),
  })

  useEffect(() => {
    localStorage.removeItem('app-token');
  }, []);

  function handleSubmit(values) {
    sendLogin(values);
  }

  const renderForm = () => {
    return (
      <Formik
        initialValues={{ }}
        onSubmit={ handleSubmit }
        validationSchema={ validations }
      >
        <Form
          className={ classes.form }
        >
          <div className={ classes.formGroup }>
            <Field
              name="email"
              placeHolder="E-mail"
              className={
                classNames(
                  classes.formField,
                  classes.loginField
                )
              }
            />
            <ErrorMessage
              component="span"
              name="email"
              className={ classes.loginError }
            />
          </div>
          <div
            className={ classes.formGroup }
          >
            <Field
              name="password"
              placeHolder="Senha"
              className={
                classes.formField
              }
            />
            <ErrorMessage
              component="span"
              name="password"
              className={ classes.loginError }
            />
          </div>
          <Button
            className={ classes.formBtn }
            type="submit"
          >
            Entrar
          </Button>
        </Form>
      </Formik>
    );
  }

  return (
    <Grid
      container
      xs={12}
      className="loginBg"
    >
      <Grid
        container
        justify="center"
        alignItems="center"
        xs={12}
        sm={4}
        className={
          classes.loginContainer
        }
      >
        <Grid item container xs={12} direction="column" sm={8} style={{ padding: '1rem' }}>
          <Box display="flex" style={{ marginBottom: '1.3rem' }}>
            <Typography component="span" className={ classes.logo }>
              <Logo />
            </Typography>
            <Typography
              variant="inherit"
              className={ classes.titleTypo }
            >
              Books
            </Typography>
          </Box>
          { renderForm() }
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Login;
