import { Grid, Typography, Button, makeStyles, Box } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import BookCard from '../components/common/BookCard';
import { getBookDetail, getBooks } from '../services/api';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import { isMobile } from 'react-device-detect';
import './Home.css';
import FilledLogo from '../assets/img/FilledLogo';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { Redirect } from 'react-router';
import { Modal } from '@material-ui/core';
import { Fade } from '@material-ui/core';
import { Backdrop } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  titleTypo: {
    fontWeight: 300,
    fontSize: '28px',
    lineHeight: '40px',
    color: '#333333',
    marginLeft: '1rem',
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    height: isMobile ? '85vh' : '',
    overflowY: 'scroll',
    padding: '3rem 0',
    background: '#FFFFFF',
    boxShadow: '0px 16px 80px rgba(0, 0, 0, 0.32)',
    borderRadius: '4px',
  },
  bookDetailDescription: {
    fontSize: '12px',
    lineHeight: '20px',
    color: '#333333',
  },
  aboutSections: {
    margin: '.8rem 0',
  }
}));

const Home = () => {
  const [bookList, setBookList] = useState([]);
  const [page, setPage] = useState(1);
  const [loginRedirectTrigger, setLoginRedirectTrigger] = useState(false);
  const [bookDetail, setBookDetail] = useState();
  const [openModal, setOpenModal] = useState(false);
  const classes = useStyles();

  useEffect(() => {
    getBooks(setBookList, page);
  }, [page])

  const handleNextPageClick = () => {
    setPage(page + 1);
  }

  const handlePreviousPageClick = () => {
    setPage(page - 1);
  }

  const handleExitClick = () => {
    localStorage.removeItem('app-token');
    setLoginRedirectTrigger(true);
  }

  const clickBookDetail = async (id) => {
    await getBookDetail(id, setBookDetail)
    setOpenModal(true);
  }

  const handleCloseModal = () => {
    setOpenModal(false);
  }

  return (
    <Grid
      container
      alignItems="center"
      xs={12}
      justify="center"
      className="homeBg"
    >
      <Grid
        item
        container
        justify="space-between"
        alignItems="center"
        spacing={3}
        xs={12}
        sm={8}
        style={{ margin: '1rem' }}
      >
        <Box display="flex" alignItems="center">
          <FilledLogo />
          <Typography variant="inherit" className={ classes.titleTypo }>
            Books
          </Typography>
        </Box>
        <Box display="flex" alignItems="center">
          <Typography variant="inherit" className={ '' } style={{ display: isMobile ? 'none' : '', }}>
            Bem vindo, Guilherme
          </Typography>
          <Button style={{ marginLeft: '.5rem' }} onClick={ () => handleExitClick() }>
            <ExitToAppIcon />
            {
              loginRedirectTrigger
              ? (
                <Redirect to="/login" />
              ) : null
            }
          </Button>
        </Box>
      </Grid>
      <Grid
        item
        container
        justify={isMobile ? "center" : null}
        alignItems="center"
        spacing={3}
        xs={12}
        sm={8}
      >
        {
          bookList.data
          ? (
            bookList.data.map((book) =>
              <Grid item container xs={10} md={3}>
                <BookCard clickAction={(id) => clickBookDetail(id)} book={ book } />
              </Grid>
            )
          ) : null
        }
      </Grid>
      <Grid
        item
        container
        justify={ isMobile ? "center" : "flex-end" }
        alignItems="center"
        xs={12}
        sm={8}
        style={{
          margin: '1rem 0',
        }}
      >
        <Typography variant="subtitle1" color="initial">
          Página { page } de 100
        </Typography>
        <Button
          onClick={() => handlePreviousPageClick()}
          color="primary"
          disabled={ page === 1 ? true : false }
        >
          <NavigateBeforeIcon />
        </Button>
        <Button
          onClick={() => handleNextPageClick()}
          color="primary"
        >
          <NavigateNextIcon />
        </Button>
      </Grid>
      {
        bookDetail
        ? (
          <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={ classes.modal }
            open={ openModal }
            onClose={ handleCloseModal }
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500,
            }}
          >
            <Fade in={openModal}>
              <Grid container xs={11} md={6} className={ classes.paper }>
                <Grid
                  item
                  container
                  xs={12}
                  md={6}
                  justify="center"
                  alignItems="center"
                >
                  <img
                    src={ bookDetail.imageUrl }
                    alt="bookImage"
                    style={{
                      width: isMobile ? '80vw' : '',
                    }}
                  />
                </Grid>
                <Grid
                  item
                  container
                  xs={12}
                  md={6}
                  direction="column"
                  justify="space-between"
                  style={{
                    padding: '0 .7rem',
                  }}
                >
                  <section className={ classes.aboutSections }>
                    <Typography
                      variant=""
                      style={{
                        fontWeight: 500,
                        fontSize: '48px',
                        lineHeight: '40px',
                        color: '#333333',
                      }}
                    >
                      { bookDetail.title }
                    </Typography>
                      { console.log(bookDetail.authors.join(', ')) }
                    <Typography
                      variant="body1"
                      style={{
                        fontWeight: 'normal',
                        fontSize: '12px',
                        lineHeight: '20px',
                        color: '#AB2680',
                      }}
                    >
                      { bookDetail.authors.join(', ') }
                    </Typography>
                  </section>
                  <section className={ classes.aboutSections }>
                    <Typography variant="h6" color="initial">
                      Informações
                    </Typography>
                    <Typography variant="body1" color="initial">
                      Páginas {bookDetail.pageCount}
                    </Typography>
                    <Typography variant="body1" color="initial">
                      Editora {bookDetail.publisher}
                    </Typography>
                    <Typography variant="body1" color="initial">
                      Publicação {bookDetail.published}
                    </Typography>
                    <Typography variant="body1" color="initial">
                      Idioma {bookDetail.language}
                    </Typography>
                    <Typography variant="body1" color="initial">
                      Título Original {bookDetail.title}
                    </Typography>
                    <Typography variant="body1" color="initial">
                      ISBN-10 {bookDetail.isbn10}
                    </Typography>
                    <Typography variant="body1" color="initial">
                      ISBN-13 {bookDetail.isbn13}
                    </Typography>
                  </section>
                  <section className={ classes.aboutSections }>
                    <Typography variant="h6" color="initial"
                      style={{
                        fontWeight: 700,
                        fontSize: '15px',
                        lineHeight: '20px',
                        textTransform: 'uppercase',
                        color: '#333333',
                      }}
                    >
                      RESENHA DA EDITORA
                    </Typography>
                    <Typography variant="body1" color="initial" className={ classes.bookDetailDescription }>
                      { bookDetail.description }
                    </Typography>
                  </section>
                </Grid>
              </Grid>
            </Fade>
          </Modal>
        ) : null
      }
    </Grid>
  );
};

export default Home;
