import React from 'react';
import ReactDOM from 'react-dom';
import 'normalize.css';
import CssBaseline from '@material-ui/core/CssBaseline';
import './index.css';
import App from './container/App';

ReactDOM.render(
  <React.StrictMode>
    <CssBaseline>
      <App />
    </CssBaseline>
  </React.StrictMode>,
  document.getElementById('root')
);
