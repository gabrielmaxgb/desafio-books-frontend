import React from 'react';
import { Route, Router, Switch, Redirect } from "react-router-dom";
import notFound404 from './components/notFound404';
import { history } from './history';
import Home from './pages/Home';
import Login from './pages/Login';
import PrivateRoute from './components/PrivateRoute';

const Routes = () => {
  return (
    <Router history={ history }>
      {
        localStorage.getItem("app-token") === null
        ? (
          <Redirect to="/login" />
        ) : null
      }
      <Switch>
        <Route exact path="/login" component={ Login }/>
        <PrivateRoute exact path="/home" component={ Home }/>
        <PrivateRoute path="*" component={ notFound404 }/>
      </Switch>
    </Router>
  );
}

export default Routes;